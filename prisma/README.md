1. For development and production we are using prisma-graphql demo mysql server on https://app.prisma.io/. If you do not wish to use prisma cloud you can set up a prisma-graphql server on Kubernetes using helm chart. You can also set up a prisma-graphql server locally using docker-compose. However here we not use local prisma-graphql server using docker-compose

Note: If you wish to setup up a prisma-graphql server with mongodb, then you will have to set up your own prisma-grapqhl server in Digital Ocean or any other platform. https://app.prisma.io/ only provides demo prisma-grapqhql server with mysql.

3. Deploy prisma schema to the necessary environment that you wish to work. The below command deploys the schema or creates the necessary tables to the endpoint provided in prisma.yml file

   prisma deploy -e ../config/prod.env    - production environment
   prisma deploy -e ../config/dev.env     - development environment

4. Accessing the prisma server

   The development playground or production playground can be accessed from https://app.prisma.io/
