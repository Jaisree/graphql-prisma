const Mutation = {

    /**
     * 
     * create user
     * 
     * 
     * mutation {
          createUser(    
            data: {
              name: "TEST"
              age: 32
              sex: "male"
              email: "test@gmail.com"
            }
          ){
              id
              name
              email
              age
           }
        }
     * 
     */

    async createUser(parent, args, { prisma }, info) {
      const emailTaken = await prisma.exists.User({ email: args.data.email })

      if (emailTaken) {
        throw new Error('Email Taken')
      }

      const user = await prisma.mutation.createUser({ data: args.data }, info)

      return prisma.mutation.createUser({ 
        data: args.data
      }, info)
    },
    
    /**
     * 
     * Update User
     * mutation {
          updateUser(
            id: "ck4nzouefj9r80922hojeyood",
            data: {
            name: "praveen"
            email: "praveen@gmail.com"
          }
       ){
          id
          name
          email
          age
        }
      }
     * 
     * 
     * 
    */

    async updateUser(parent, args, { prisma }, info ) {
       return prisma.mutation.updateUser({
         where: {
           id: args.id
         },
         data: args.data
       }, info)
     },

     /**
      * 
      * Delete User
      * 
      *   mutation {
            deleteUser (id: "ck4nzouefj9r80922hojeyood") {
              id
              name
            }
          }

      */

     async deleteUser(parent, args, { prisma }, info ) {
       const userExists = await prisma.exists.User({ id: args.id })

       if(!userExists) {
         throw new Error('user not found')
       }

      return prisma.mutation.deleteUser({
        where: {
          id: args.id
        }
      }, info)
    }
}

export { Mutation as default }