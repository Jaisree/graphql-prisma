const Query = { 
  
  /**
   * 
   * query {
       users {
         id
         name
       }
     }
     
   */
    users(parent, args, { prisma }, info) {
      return prisma.query.users(null, info)
    }       
}

export { Query as default }