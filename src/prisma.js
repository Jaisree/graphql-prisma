
// Here we write the necessary code to connect to prisma-graphql api

// prisma-binding gives you a set of methods to interact with priama-graphql api

import { Prisma } from 'prisma-binding'

// Instantiate `Prisma` based on concrete service
const prisma = new Prisma({
    typeDefs: 'src/generated/prisma.graphql',
    endpoint: process.env.PRISMA_ENDPOINT
})

export { prisma as default }



