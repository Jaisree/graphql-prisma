import '@babel/polyfill'
import { GraphQLServer } from 'graphql-yoga'
import db from './db'
import Query from './resolvers/Query'
import Mutation from './resolvers/Mutation'
import prisma from './prisma'
//import Mutation from './resolvers/Mutation'
//import User from './resolvers/User'
//import Post from './resolvers/Post'
//import Comment from './resolvers/Comment'

// ... or using `require()`
// const { GraphQLServer } = require('graphql-yoga')

const server = new GraphQLServer({ 
    typeDefs: './src/schema.graphql', 
    resolvers: {
      Query,
      Mutation
      //User,
      //Post,
      //Comment
    },
    context: {
      db,
      prisma  
    } 
})

server.start(() => console.log('Server is running on http://localhost:4000'))