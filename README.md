# Introduction:

Here graphql-yoga or nodejs acts as a front-end for prisma-graphql. prisma-binding is a npm module for gr
aphql-yoga or nodejs which provides you with methods to perform CRUD operations on graphql-prisma-mongodb
. The application does not directly access the database but through prisma-graphql-yoga or nodejs
Steps:

# Running on development environment:

1. npm install
2. npm run get-schema (It generates a schema from the prisma schema file. This schema will be used by graphql Yoga which works as a nodejs front-end for prisma-graphql api. If you make a change to the prisma schema i.e add a field or a table/query or mutation then you will have to run this command to fetch the latest changes)
2. npm run dev

# Running the application as a Docker (For production environment only)

1. docker build -t graphql-heroku:latest .
2. docker run -d -p 4000:4000 graphql-heroku

# Push Docker image to Heroku

1. heroku container:login
2. heroku create <PROVIDE APP NAME HERE>
3. heroku container:push web --app <PROVIDE APP NAME HERE>
4. heroku container:release web --app <PROVIDE APP NAME HERE>
5. Access the application on https://<APP NAME HERE>.herokuapp.com
