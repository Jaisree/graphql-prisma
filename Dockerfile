FROM node:10

WORKDIR /app

RUN npm install -g prisma@1.34

COPY . .

RUN npm install

RUN npm build

EXPOSE 4000

CMD [ "npm", "start" ]